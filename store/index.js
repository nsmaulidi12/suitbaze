export const state = () => ({
	isShow: true
})

export const mutations = {
	toggled(state) {
		state.isShow = !state.isShow
	}
}
